using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

[RequireComponent(typeof(AudioSource))]
public class AudioManager : MonoBehaviour
{
    public GameObject PlayButton;
    public GameObject PauseButton;
    public Slider Slider;

    public AudioClip[] playlist;
    private int currentMusic;
    private AudioSource audioSource;
    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        audioSource.clip = playlist[0];
        currentMusic = 0;
    }

    public void MusicPlay()
    {
        if(!audioSource.isPlaying && audioSource.time != 0)
        {
            audioSource.UnPause();
        }
        else
        {
            audioSource.Play();
        }
        StartCoroutine(WaitEndMuzsic());
    }

    IEnumerator WaitEndMuzsic()
    {
        while(audioSource.isPlaying)
        {
            PlayButton.SetActive(false);
            PauseButton.SetActive(true);
            yield return null;
        }
        NextMusic();
    }

    public void MusicPause()
    {
        audioSource.Pause();
        StopAllCoroutines();
    }

    public void NextMusic()
    {
        audioSource.Stop();
        currentMusic++;
        if (currentMusic > playlist.Length - 1)
        {
            currentMusic = 0;
        }
        audioSource.clip = playlist[currentMusic];
        audioSource.Play();
        StartCoroutine(WaitEndMuzsic());
    }

    public void PrevMusic()
    {
        audioSource.Stop();
        currentMusic--;
        if (currentMusic < 0)
        {
            currentMusic = playlist.Length - 1;
        }
        audioSource.clip = playlist[currentMusic];
        audioSource?.Play();
        StartCoroutine(WaitEndMuzsic());
    }
}
