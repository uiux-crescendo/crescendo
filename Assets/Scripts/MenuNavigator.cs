using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class MenuNavigator : MonoBehaviour
{
    public void ChangeScene()
    {
        Debug.Log(this.gameObject.name);

        switch(this.gameObject.name)
        {
            case "MusicButton":
                SceneManager.LoadScene("Nulan");
                break;
            
            case "HomeButton":
                SceneManager.LoadScene("Accueil");
                break;

            case "HomeButtonOff":
                SceneManager.LoadScene("Accueil");
                break;

            case "SearchButton":
                SceneManager.LoadScene("Recherche");
                break;

            case "SearchButtonOff":
                SceneManager.LoadScene("Recherche");
                break;

             case "NotificationButton":
                SceneManager.LoadScene("Notification");
                break;

            case "NotificationButtonOff":
                SceneManager.LoadScene("Notification");
                break;

            case "UserButton":
                SceneManager.LoadScene("User");
                break;

            case "UserButtonOff":
                SceneManager.LoadScene("User");
                break;

           case "ResearchProfileButton":
                SceneManager.LoadScene("UserPage");
                break;

            case "InfoUserButton":
                SceneManager.LoadScene("UserPage");
                break;

            case "ResearchSongButton":
                SceneManager.LoadScene("Nulan");
                break;

            case "SearchNewFriends":
                SceneManager.LoadScene("Recherche");
                break;

            case "PostButton1":
                SceneManager.LoadScene("Post");
                break;

             case "BackButtonPost":
                SceneManager.LoadScene("Accueil");
                break;
                
            default:
                break;
        }
        //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex +1);
    }
}
